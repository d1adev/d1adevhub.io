var console = 'n';
var players = 'n';
var mode = 'n';

function chose(v) {
    console = v;
    switchToOptions();
}

function switchToOptions() {
    $('#buttons').animate({
        marginTop: '-20%'
    }, 800, function () {
        $('#buttons').remove();
        addOptions();
    });
}

function addOptions() {
    $('#fullOptionsDiv').slideDown("slow");
}

function choseMode(v) {
    if (mode != 'n') {
        if (mode == '1') {
            $('#buttonMode1').animate({
                backgroundColor: '#2c3e50'
            }, 500);
        }
        if (mode == '2') {
            $('#buttonMode2').animate({
                backgroundColor: '#2c3e50'
            }, 500);
        }
        if (mode == '3') {
            $('#buttonMode3').animate({
                backgroundColor: '#2c3e50'
            }, 500);
        }
        if (mode == '4') {
            $('#buttonMode4').animate({
                backgroundColor: '#2c3e50'
            }, 500);
        }
    }
    if (v == '1') {
        mode = '1';
        $('#buttonMode1').animate({
            backgroundColor: 'green'
        }, 500);
    } else if (v == '2') {
        mode = '2';
        $('#buttonMode2').animate({
            backgroundColor: 'green'
        }, 500);
    } else if (v == '3') {
        mode = '3';
        $('#buttonMode3').animate({
            backgroundColor: 'green'
        }, 500);
    } else if (v == '4') {
        mode = '4';
        $('#buttonMode4').animate({
            backgroundColor: 'green'
        }, 500);
    } else {
        mode = 'n';
    }
}

function chosePlayer(v) {
    if (players != 'n') {
        if (players == '1') {
            $('#buttonPlayer1').animate({
                backgroundColor: '#2c3e50'
            }, 500);
        }
        if (players == '2') {
            $('#buttonPlayer2').animate({
                backgroundColor: '#2c3e50'
            }, 500);
        }
        if (players == '3') {
            $('#buttonPlayer3').animate({
                backgroundColor: '#2c3e50'
            }, 500);
        }
        if (players == '4') {
            $('#buttonPlayer4').animate({
                backgroundColor: '#2c3e50'
            }, 500);
        }
    }
    if (v == '1') {
        players = '1';
        $('#buttonPlayer1').animate({
            backgroundColor: 'green'
        }, 500);
    } else if (v == '2') {
        players = '2';
        $('#buttonPlayer2').animate({
            backgroundColor: 'green'
        }, 500);
    } else if (v == '3') {
        players = '3';
        $('#buttonPlayer3').animate({
            backgroundColor: 'green'
        }, 500);
    } else if (v == '4') {
        players = '4';
        $('#buttonPlayer4').animate({
            backgroundColor: 'green'
        }, 500);
    } else {
        players = 'n';
    }
}

function openMatches() {
    $('#fullOptionsDiv').animate({
        marginTop: '-80%'
    }, 800, function () {
        $('#fullOptionsDiv').remove();
        addTable();
    });
}

function addTable() {
    $('#tablePls').load("table.php");
}